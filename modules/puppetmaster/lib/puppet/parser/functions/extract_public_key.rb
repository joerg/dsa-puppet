module Puppet::Parser::Functions
  newfunction(:extract_public_key, :type => :rvalue) do |args|
    require 'openssl'

    source_file = args.shift()
    include_headers = args.shift()

    res = []
    if File.exist?(source_file)
      begin
        keys = OpenSSL::PKey::RSA.new(File.read(source_file))
      rescue OpenSSL::PKey::RSAError
        keys = OpenSSL::X509::Certificate.new(File.read(source_file))
      end
      pubkey = keys.public_key.export
      pubkey.each_line do |line|
        unless line.start_with?("---") and not include_headers
          res << line.chomp
        end
      end
      res << ''
    else
        res << "; key / cert file #{source_file} did not exist to extract public key"
    end

    return res.join("\n")
  end
end
