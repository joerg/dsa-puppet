# Bacula sd config: director snippet
#
# Each/The director exports this class to be collected by each/the storage.
#
# @param director_name     bacula name of the dir instance
# @param director_address  address of this dir instance that other instances should connect to (dns name)
# @param messages_name   name of the Messages Resource
define bacula::storage::director(
  String $director_name,
  Stdlib::Host $director_address,
  String $messages_name,
) {
  include bacula::storage

  # this is created in both bacula::storage::client and
  # bacula::storage::director and needs to be the same
  $dir_storage_secret = hkdf('/etc/puppet/secret', "bacula::director<->storage::${director_address}<->${::fqdn}")

  file {
    "/etc/bacula/storage-conf.d/Dir_${director_address}.conf":
      content => template('bacula/storage/sd-per-director.conf.erb'),
      mode    => '0440',
      group   => bacula,
      notify  => Exec['bacula-sd restart-when-idle'],
      ;
  }
}
