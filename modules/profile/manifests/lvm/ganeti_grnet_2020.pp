# LVM config for the lenovo x86 servers that make up ganeti.grnet.debian.org
class profile::lvm::ganeti_grnet_2020 {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
