# LVM config for the ppc hosts that make up ganeti2-osuosl.debian.org
class profile::lvm::ganeti2_osuosl {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
