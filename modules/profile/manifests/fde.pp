# this configures (partly) full disk encryption on hosts
#
# for now, it only ensures the initramfs loads dropbear so we can
# unlock the filesystems on boot.
#
# this derives the profile::admins class so it can override the
# User['root'] parameter.
#
# @param extra_authorized_keys extra keys on top of the keys provided
#                              by profile::admins::keys
#
# @param ensure present/absent
class profile::fde(
  Hash $extra_authorized_keys = {},
  Enum['present', 'absent'] $ensure = 'present',
) {
  file { '/etc/dropbear-initramfs/dropbear_dss_host_key':
    ensure => 'absent',
  }

  class { 'dropbear_initramfs':
    ensure  => $ensure,
    content => template('ssh/authorized_keys-dropbear.erb'),
  }
}
