# do not modify - this file is maintained via puppet

AddType application/font-woff2 .woff2

Use common-debian-service-https-redirect * debconf21.debconf.org

WSGIDaemonProcess debconf21 \
  processes=3 threads=2 \
  user=www-data group=debconf-web maximum-requests=750 umask=0007 display-name=wsgi-debconf21.debconf.org \
  python-path=/srv/debconf-web/debconf21.debconf.org/dc21/:/srv/debconf-web/debconf21.debconf.org/dc21/ve/lib/python3.7/site-packages/

<VirtualHost *:443>
  ServerAdmin admin@debconf.org
  ServerName debconf21.debconf.org

  ErrorLog  /var/log/apache2/debconf21.debconf.org-error.log
  CustomLog /var/log/apache2/debconf21.debconf.org-access.log combined

  Use common-debian-service-ssl debconf21.debconf.org
  Use common-ssl-HSTS

  Header always set Referrer-Policy "same-origin"
  Header always set X-Content-Type-Options nosniff
  Header always set X-XSS-Protection "1; mode=block"
#  Header always set Access-Control-Allow-Origin: "*"

  WSGIProcessGroup debconf21
  WSGIScriptAlias / /srv/debconf-web/debconf21.debconf.org/dc21/wsgi.py
  WSGIPassAuthorization On

  <Directory /srv/debconf-web/debconf21.debconf.org/dc21>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  Alias /static/ /srv/debconf-web/debconf21.debconf.org/dc21/localstatic/
  Alias /favicon.ico /srv/debconf-web/debconf21.debconf.org/dc21/localstatic/img/favicon/favicon.ico
  <Directory /srv/debconf-web/debconf21.debconf.org/dc21/localstatic/>
    Require all granted

    # A little hacky, but it means we won't accidentally catch non-hashed filenames
    <FilesMatch ".*\.[0-9a-f]{12}\.[a-z0-9]{2,5}$">
      ExpiresActive on
      ExpiresDefault "access plus 1 year"
    </FilesMatch>
  </Directory>

  Alias /media/ /srv/debconf-web/debconf21.debconf.org/dc21/media/
  <Directory /srv/debconf-web/debconf21.debconf.org/dc21/media/>
    Require all granted
  </Directory>
</VirtualHost>

# vim: set ft=apache:
