# The sreview class.  sreview is the conference talk video reviewing service
#
# @param user  username of the sreview role user
class roles::sreview (
  String $user = 'sreview',
) {
  include apache2
  ssl::service { 'sreview.debian.net': notify  => Exec['service apache2 reload'], key => true, }

  dsa_systemd::linger { $user: }

  # enable copying files from the sreview host to the video_archive host.
  ssh::keygen { $user: }
  ssh::authorized_key_add { "sreview_to_video_archive::${user}":
    target_user => 'videoteam',
    command     => 'bin/ssh-from-sreview-wrap',
    key         => dig($facts, 'ssh_keys_users', $user, 'id_rsa.pub', 'line'),
    collect_tag => 'sreview_to_video_archive',
  }
}
