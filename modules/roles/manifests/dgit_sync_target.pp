# class to collect the ssh keys sent by the dgit host on the browse and
# (public) git host
class roles::dgit_sync_target {
  ssh::authorized_key_collect { 'dgit-sync':
    target_user => 'dgit-unpriv',
    collect_tag => 'roles::dgit::sync'
  }
}
