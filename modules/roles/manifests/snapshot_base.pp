# just a base class for snapshot things
class roles::snapshot_base {
  ensure_packages ( [
    'build-essential',
    'python-dev',
    'libssl-dev',
  ], { ensure => 'installed' })
}
