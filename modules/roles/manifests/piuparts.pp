class roles::piuparts {
  include apache2
  ssl::service { 'piuparts.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
}
