class roles::cdbuilder {
  # debian-cd wants to make hardlinks to files it doesn't own; let it.
  file { '/etc/sysctl.d/protect-links.conf':
    ensure => absent,
  }
  base::sysctl { 'cdimage-software-really-needs-to-be-fixed':
    key   => 'fs.protected_hardlinks',
    value => '0',
  }

}
