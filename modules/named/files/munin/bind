#!/usr/bin/perl -w
#
# Plugin to monitor usage of bind 9 servers
#

use strict;

my $logfile=$ENV{'logfile'} || '/var/log/daemon.log';
my $rotlogfile;
my $pos = undef;

my $use_views = ($0 =~ /views$/) ? 1 : 0;
my $STATEFILE = $use_views ? "$ENV{MUNIN_PLUGSTATE}/bind9-views.state" : "$ENV{MUNIN_PLUGSTATE}/bind9.state";
my $OTHER=0;
my %IN;

if (defined($ARGV[0]) and ($ARGV[0] eq 'config')) {
  &do_config;
  exit(0);
}

if (-f "$logfile.0") {
  $rotlogfile = $logfile . ".0";
} elsif (-f "$logfile.1") {
  $rotlogfile = $logfile . ".1";
} elsif (-f "$logfile.01") {
  $rotlogfile = $logfile . ".01";
} else {
  $rotlogfile = $logfile . ".0";
}

exit 0 if(! -f $logfile && ! -f $rotlogfile);

&get_state;

my $startsize = (stat $logfile)[7];

if(!defined $pos) {
    # Initial run.
    $pos = $startsize;
}

if($startsize < $pos) {
    # Log rotated
    &parselog($rotlogfile, $pos, (stat $rotlogfile)[7]);
    $pos = 0;
}

&parselog($logfile, $pos, $startsize);
foreach my $k (keys %IN) {
  print "query_$k.value ",$IN{$k},"\n";
}
$pos = $startsize;

print "query_other.value ",$OTHER,"\n";
open(Q,">$STATEFILE") or die "WTF: state no writey: $!\n";
print Q "$logfile:$pos\n";
foreach my $k (keys %IN) {
  print Q "$k\n";
}
close Q;

sub parselog {
  my($fname, $start, $stop) = @_;
  my $match;
  if ($fname eq '/var/log/daemon.log') {
    $match = qr/named\[\d+\]: client/;
  } else {
    $match = qr/./;
  }
  open(LOG, $fname) or die "WTF: log: $!\n";
  seek(LOG, $start, 0) or die "Odd: seek: $!\n";

  # With view:
  #     0             1         2       3          4                5            6                     7    8   9      10                  11 12    13     14
  # 11-Mar-2020 11:15:48.455 queries: client @0x7f3e3803c360 217.114.0.60#50701 (security.debian.org): view EU: query: security.debian.org IN AAAA -E(0)DC (82.195.75.105)
  # without:
  #     0             1         2       3          4                5            6                     7      8                   9  10   11      12
  # 11-Mar-2020 11:15:48.455 queries: client @0x7f3e3803c360 217.114.0.60#50701 (security.debian.org): query: security.debian.org IN AAAA -E(0)DC (82.195.75.105)

  while (tell(LOG) < $stop) {
    my $line = <LOG>;
    if ($line =~ /$match/) {
      chomp $line;
      my @array = split(/\s+/, $line);
      my $off = ($array[7] eq 'view') ? 2 : 0;
      my $c = $array[9 + $off];
      my $q = $use_views ? $array[8] : $array[10 + $off];
      $q =~ s/:$//;
      if ($c eq 'IN') {
        $IN{$q}++;
      } else {
        $OTHER++;
      }
    }
  }
  close(LOG);
}

sub get_state {
  open(Q,"< $STATEFILE") or return;
  while (<Q>) {
    if (/^$logfile:(\d+)/) {
      $pos = $1;
      next
    } else {
      chomp;
      $IN{$_} = 0 unless(defined($IN{$_}));
    }
  }
  close Q;    
}

sub do_config {
  printf "graph_title DNS Queries by %s
graph_vlabel Queries / \${graph_period}
", ($0 =~ /views$/ ? 'view' : 'type');
  &get_state;
  foreach my $k (sort keys %IN) {
    print "query_$k.label $k
query_$k.type DERIVE
query_$k.min 0
";
  }
  print "query_other.label Other
query_other.type DERIVE
query_other.min 0
";

};

# vim:syntax=perl

